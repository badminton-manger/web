export default [
  {
    path: '/enroll/single',
    component:{template:'<router-view/>'},
    children:[
      {
        path: 'step1',
        component: () => import('@/view/enroll/single/step1')
      },
      {
        path: 'step2',
        component: () => import('@/view/enroll/single/step2')
      },
      {
        path: 'step3',
        component: () => import('@/view/enroll/single/step3')
      },
      {
        path: 'step4',
        component: () => import('@/view/enroll/single/step4')
      },
      {
        path: 'step5',
        component: () => import('@/view/enroll/single/step5')
      },
      {
        path: 'step6',
        component: () => import('@/view/enroll/single/step6')
      }
    ]
  },
  {
    path: '/enroll/team',
    component:{template:'<router-view/>'},
    children:[
      {
        path: 'step1',
        component: () => import('@/view/enroll/team/step1')
      },
      {
        path: 'step2',
        component: () => import('@/view/enroll/team/step2')
      },
      {
        path: 'step3',
        component: () => import('@/view/enroll/team/step3')
      },
      {
        path: 'step4',
        component: () => import('@/view/enroll/team/step4')
      },
      {
        path: 'step5',
        component: () => import('@/view/enroll/team/step5')
      },
      {
        path: 'step6',
        component: () => import('@/view/enroll/team/step6')
      }
    ]
  },
  {
    path: '/enroll/group',
    component:{template:'<router-view/>'},
    children:[
      {
        path: 'step1',
        component: () => import('@/view/enroll/group/step1')
      },
      {
        path: 'step2',
        component: () => import('@/view/enroll/group/step2')
      },
      {
        path: 'step3',
        component: () => import('@/view/enroll/group/step3')
      },
      {
        path: 'step4',
        component: () => import('@/view/enroll/group/step4')
      },
      {
        path: 'step5',
        component: () => import('@/view/enroll/group/step5')
      }
    ]
  },

]
