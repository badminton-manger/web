var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

//通过匹配调用api接口的参数，返回相应的本地JSON文件内容
router.all('/api/*', function(req, res, next) {
	var path = require('path');
	var fs = require('fs');
	var str = req.path.substr(5);
	var file = path.join(__dirname, '../mock/' +  str + ".json");
	fs.readFile(file, 'utf-8', function(err, data) { 
		if (err) {
            res.json({message:'数据加载失败',status:0});
        } else {
            res.send({message:'数据加载成功',status:1,data: JSON.parse(data)});
            // res.sendStatus(500);
        }
	});
});

router.post('/rest/match/list', function(req, res, next) {
	res.json({
	  	"pageNo": 0,
	    "pageSize": 0,
	    "total": 4,
	    "rows":[
			{
	            "id": 1,
	            "matchName": "双雄会",
	            "kind": "MIXMATCH",
	            "starttime": "2019-07-03 17:19:25",
	            "endtime": "2019-07-10 17:19:32",
	            "status": "ENTERING",
	            "saletotal": 121,
	            "matchAddress": "羽星公园",
	            "overtime": "2019-07-04 17:20:15",
	            "level":[
				    {
				        "name": "单项赛",
				        "children": [
				            {
				                "data": {
				                    "ageMin": "",
				                    "ageMax": "",
				                    "ageTotal": "",
				                    "projects": [
				                        "MANDOUBLE",
				                        "MANSINGLE",
				                        "MIXDOUBLE",
				                        "WOMENDOUBLE",
				                        "WOMENSINGLE"
				                    ]
				                },
				                "name": "青年组",
				                "children": []
				            },
				            {
				                "data": {
				                    "ageMin": "",
				                    "ageMax": "",
				                    "ageTotal": "",
				                    "projects": [
				                        "MANDOUBLE",
				                        "MANSINGLE",
				                        "MIXDOUBLE",
				                        "WOMENDOUBLE",
				                        "WOMENSINGLE"
				                    ]
				                },
				                "name": "成年组",
				                "children": []
				            }
				        ],
				        "parentchild": [
				            {
				                "parentAgeMin": "",
				                "parentAgeMax": "",
				                "childrenAgeMin": "",
				                "childrenAgeMax": "",
				                "name": "父女组",
				                "parentSex": "male",
				                "childrenSex": "female"
				            }
				        ]
				    },
				    {
				        "data": {
				            "ageMin": "20",
				            "ageMax": "29",
				            "ageTotal": "70",
				            "projects": [
				                "MANDOUBLE",
				                "MANSINGLE",
				                "MIXDOUBLE",
				                "WOMENDOUBLE",
				                "WOMENSINGLE",
				                "UNLIMITDOUBLE"
				            ]
				        },
				        "name": "团体赛",
				        "children": [],
				        "parentchild": [
				            {
				                "parentAgeMin": "30",
				                "parentAgeMax": "39",
				                "childrenAgeMin": "10",
				                "childrenAgeMax": "19",
				                "name": "父女组",
				                "parentSex": "male",
				                "childrenSex": "female"
				            }
				        ]
				    }
				]
	        },	        
			{
	            "id": 2,
	            "matchName": "双雄会",
	            "kind": "SIGNMATCH",
	            "starttime": "2019-07-03 17:19:25",
	            "endtime": "2019-07-10 17:19:32",
	            "status": "ENTEROVER",
	            "saletotal": 121,
	            "matchAddress": "羽星公园",
	            "overtime": "2019-07-04 17:20:15",
	            "level":[
				    {
				        "name": "单项赛",
				        "children": [
				            {
				                "data": {
				                    "ageMin": "",
				                    "ageMax": "",
				                    "ageTotal": "",
				                    "projects": [
				                        "MANDOUBLE",
				                        "MANSINGLE",
				                        "MIXDOUBLE",
				                        "WOMENDOUBLE",
				                        "WOMENSINGLE"
				                    ]
				                },
				                "name": "青年组",
				                "children": []
				            },
				            {
				                "data": {
				                    "ageMin": "",
				                    "ageMax": "",
				                    "ageTotal": "",
				                    "projects": [
				                        "MANDOUBLE",
				                        "MANSINGLE",
				                        "MIXDOUBLE",
				                        "WOMENDOUBLE",
				                        "WOMENSINGLE"
				                    ]
				                },
				                "name": "成年组",
				                "children": []
				            }
				        ],
				        "parentchild": [
				            {
				                "parentAgeMin": "",
				                "parentAgeMax": "",
				                "childrenAgeMin": "",
				                "childrenAgeMax": "",
				                "name": "父女组",
				                "parentSex": "male",
				                "childrenSex": "female"
				            }
				        ]
				    }
				]
	        },
	        {
	            "id": 3,
	            "matchName": "双雄会",
	            "kind": "GROUPMATCH",
	            "starttime": "2019-07-03 17:19:25",
	            "endtime": "2019-07-10 17:19:32",
	            "status": "MATCHING",
	            "saletotal": 121,
	            "matchAddress": "羽星公园",
	            "overtime": "2019-07-04 17:20:15",
	            "level":[
	            	{
				        "data": {
				            "ageMin": "20",
				            "ageMax": "29",
				            "ageTotal": "70",
				            "projects": [
				                "MANDOUBLE",
				                "MANSINGLE",
				                "MIXDOUBLE",
				                "WOMENDOUBLE",
				                "WOMENSINGLE",
				                "UNLIMITDOUBLE"
				            ]
				        },
				        "name": "团体赛",
				        "children": [],
				        "parentchild": [
				            {
				                "parentAgeMin": "30",
				                "parentAgeMax": "39",
				                "childrenAgeMin": "10",
				                "childrenAgeMax": "19",
				                "name": "父女组",
				                "parentSex": "male",
				                "childrenSex": "female"
				            }
				        ]
				    }
				]
	        }
	    ] 
	    
	});
});

module.exports = router;
