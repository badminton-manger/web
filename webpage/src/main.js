// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import axios from 'axios' 
import VueAxios from 'vue-axios'
import store from './store'
import router from './router'
import App from './App' 
import Mint from 'mint-ui'
import 'mint-ui/lib/style.css'
import '@/assets/css/base.css'
import '@/assets/css/main.css'
import '@/assets/font-awesome/css/font-awesome.min.css'
import '@/assets/font-icons/style.css'

Vue.use(VueAxios, axios)
Vue.use(Mint)
Vue.config.productionTip = false

/*
axios.interceptors.request.use(function(config){
	//在发送请求之前做某事
	console.log("拦截request")
	console.log(config)  //单次请求的配置信息对象
	return config;  //只有return config后，才能成功发送请求
},function(error){
//请求错误时做些事
	return Promise.reject(error);
});

axios.interceptors.response.use(function(data){
  console.log("拦截response")
  console.log(data)  //响应数据
  return data;   //只有return data后才能完成响应
})
*/

// Vue.prototype._site = "http://257xb54971.wicp.vip/rest/"
Vue.prototype._site = "http://192.168.1.11:8080/rest/"
// Vue.prototype._site = "http://192.168.0.103:3200/rest/"
// Vue.prototype._site = "http://192.168.8.122:3200/api/"

// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
// axios.defaults.headers['Content-Type'] = "application/json; charset=utf-8"

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { 
  	App
  },
  template: '<App/>'
})
