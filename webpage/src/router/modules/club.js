export default [
  {
    path: '/club',
    component: () => import('@/view/club')
  },
  {
    path: '/club/:id',
    component: () => import('@/view/club/details')
  }
]
