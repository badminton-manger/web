import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

import my from '@/router/modules/my'
import club from '@/router/modules/club'
import match from '@/router/modules/match'
import member from '@/router/modules/member'
import enroll from '@/router/modules/enroll'

export default new Router({
  mode: 'history',
  routes: [  
    {
      path: '/',
      component: () => import('@/view/match')
    },
    {
      path: '/test',
      component: () => import('@/view/test')
    },
    {
      path: '/login',
      component: () => import('@/view/login')
    },
    {
      path: '/register',
      component: () => import('@/view/register')
    },
    {
      path: '/register-complete',
      component: () => import('@/view/register-complete')
    },
    {
      path: '/forget',
      component: () => import('@/view/forget')
    },
    {
      path: '/forget-complete',
      component: () => import('@/view/forget-complete')
    }
  ].concat(my,club,match,member,enroll)
})
