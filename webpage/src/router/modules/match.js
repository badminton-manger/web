export default [
  {
    path: '/match',
    component: () => import('@/view/match')
  },
  {
    path: '/match/:id',
    component: () => import('@/view/match/details')
  }
]
