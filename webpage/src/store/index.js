import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

const state = {
	userId:"000001"
}

export default new Vuex.Store({
	state 
})