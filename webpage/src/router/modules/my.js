export default [
  {
    path: '/my',
    component: () => import('@/view/my')
  },
  {
    path: '/my/certification',
    component: () => import('@/view/my/certification')
  },
  {
    path: '/my/club',
    component: () => import('@/view/my/club')
  },
  {
    path: '/my/club/create',
    component: () => import('@/view/my/clubCreate')
  },
  {
    path: '/my/info',
    component:{ template:'<router-view/>' },
    children:[
      {
        path: '',
        component: () => import('@/view/my/info/index')
      },
      {
        path: 'head',
        component: () => import('@/view/my/info/head')
      },
      {
        path: 'nickname',
        component: () => import('@/view/my/info/nickname')
      },
      {
        path: 'role',
        component: () => import('@/view/my/info/role')
      },
      {
        path: 'playage',
        component: () => import('@/view/my/info/playage')
      },
      {
        path: 'strong',
        component: () => import('@/view/my/info/strong')
      },
      {
        path: 'trait',
        component: () => import('@/view/my/info/trait')
      },
      {
        path: 'mobile',
        component: () => import('@/view/my/info/mobile')
      },
      {
        path: 'password',
        component: () => import('@/view/my/info/password')
      }
    ]
  },
  {
    path: '/my/match',
    component: () => import('@/view/my/match')
  },
  {
    path: '/my/message',
    component: () => import('@/view/my/message')
  },
  {
    path: '/my/partner',
    component: () => import('@/view/my/partner')
  },
  {
    path: '/my/safe',
    component: () => import('@/view/my/safe')
  },
  {
    path: '/my/enroll',
    component: () => import('@/view/my/enroll')
  }
]
