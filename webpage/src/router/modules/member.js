export default [
  {
    path: '/member',
    component: () => import('@/view/member')
  },
  {
    path: '/member/:id',
    component: () => import('@/view/member/details')
  }
]